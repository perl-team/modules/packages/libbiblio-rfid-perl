Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Biblio-RFID
Upstream-Contact: Dobrica Pavlinusic <dpavlin@rot13.org>
Upstream-Name: Biblio-RFID

Files: *
Copyright: 2010, Dobrica Pavlinusic <dpavlin@rot13.org>
License: GPL-2+

Files: inc/Module/*
Copyright: 2002-2010, Adam Kennedy <adamk@cpan.org>
 2002-2010, Audrey Tang <autrijus@autrijus.org>
 2002-2010, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2023, Mason James <mtj@kohaaloha.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
