Source: libbiblio-rfid-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: libdata-dump-perl <!nocheck>,
                     libdevice-serialport-perl <!nocheck>,
                     libdigest-crc-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libbiblio-rfid-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libbiblio-rfid-perl.git
Homepage: https://metacpan.org/release/Biblio-RFID
Rules-Requires-Root: no

Package: libbiblio-rfid-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdata-dump-perl,
         libdevice-serialport-perl,
         libdigest-crc-perl
Description: perl tools to use different RFID readers for library use
 Biblio::RFID aims to provide a simple API to a RFID device, and then provide
 useful abstractions to write applications to respond on tags which come
 in range of RFID reader using Biblio::RFID::Reader.
 .
 Writing support for new RFID readers should be easy. Biblio::RFID::Reader::API
 provides documentation on writing support for different readers.
 .
 Currently, two serial RFID readers based on Biblio::RFID::Reader::Serial
 are implemented:
 .
   Biblio::RFID::Reader::3M810
   Biblio::RFID::Reader::CPRM02
 .
 There is also simple read-only reader using shell commands in
 Biblio::RFID::Reader::librfid.
 .
 For implementing application take a look at Biblio::RFID::Reader
 .
  scripts/RFID-JSONP-server.pl is an example of a simple application. It's
  provides an interface to a local RFID reader and a JSONP REST server.
 .
  examples/koha-rfid.js is jQuery based JavaScript code which can be inserted
  in a Koha library system to provide an overlay with tags in range and
  check-in/check-out form-fill functionality.
 .
 Applications can use Biblio::RFID::RFID501 which is some kind of
 semi-standard 3M layout or blocks on RFID tags.
